namespace CreateXML
{
    partial class FormXML
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewCSV = new System.Windows.Forms.DataGridView();
            this.buttSelAll = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.buttShowResult = new System.Windows.Forms.Button();
            this.buttGenXML = new System.Windows.Forms.Button();
            this.buttUnselAll = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.openFileCSVDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьФайлCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.CheckB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewCSV
            // 
            this.dataGridViewCSV.AllowUserToAddRows = false;
            this.dataGridViewCSV.AllowUserToDeleteRows = false;
            this.dataGridViewCSV.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.dataGridViewCSV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCSV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckB});
            this.dataGridViewCSV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCSV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewCSV.EnableHeadersVisualStyles = false;
            this.dataGridViewCSV.Location = new System.Drawing.Point(0, 24);
            this.dataGridViewCSV.MultiSelect = false;
            this.dataGridViewCSV.Name = "dataGridViewCSV";
            this.dataGridViewCSV.ReadOnly = true;
            this.dataGridViewCSV.RowHeadersVisible = false;
            this.dataGridViewCSV.RowTemplate.ReadOnly = true;
            this.dataGridViewCSV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCSV.Size = new System.Drawing.Size(417, 273);
            this.dataGridViewCSV.TabIndex = 15;
            this.dataGridViewCSV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCSV_CellClick);
            // 
            // buttSelAll
            // 
            this.buttSelAll.Location = new System.Drawing.Point(21, 14);
            this.buttSelAll.Name = "buttSelAll";
            this.buttSelAll.Size = new System.Drawing.Size(98, 24);
            this.buttSelAll.TabIndex = 16;
            this.buttSelAll.Text = "Выбрать все";
            this.buttSelAll.UseVisualStyleBackColor = true;
            this.buttSelAll.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttShowResult);
            this.panel1.Controls.Add(this.buttGenXML);
            this.panel1.Controls.Add(this.buttUnselAll);
            this.panel1.Controls.Add(this.buttSelAll);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(0, 297);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(427, 97);
            this.panel1.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(293, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 24);
            this.button1.TabIndex = 20;
            this.button1.Text = "Создать XML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttShowResult
            // 
            this.buttShowResult.Location = new System.Drawing.Point(151, 44);
            this.buttShowResult.Name = "buttShowResult";
            this.buttShowResult.Size = new System.Drawing.Size(98, 24);
            this.buttShowResult.TabIndex = 19;
            this.buttShowResult.Text = "Результат >>";
            this.buttShowResult.UseVisualStyleBackColor = true;
            this.buttShowResult.Click += new System.EventHandler(this.buttShowResult_Click);
            // 
            // buttGenXML
            // 
            this.buttGenXML.Location = new System.Drawing.Point(151, 14);
            this.buttGenXML.Name = "buttGenXML";
            this.buttGenXML.Size = new System.Drawing.Size(98, 24);
            this.buttGenXML.TabIndex = 18;
            this.buttGenXML.Text = "Создать XML";
            this.buttGenXML.UseVisualStyleBackColor = true;
            this.buttGenXML.Click += new System.EventHandler(this.buttGenXML_Click);
            // 
            // buttUnselAll
            // 
            this.buttUnselAll.Location = new System.Drawing.Point(21, 44);
            this.buttUnselAll.Name = "buttUnselAll";
            this.buttUnselAll.Size = new System.Drawing.Size(98, 24);
            this.buttUnselAll.TabIndex = 17;
            this.buttUnselAll.Text = "Сбросить все";
            this.buttUnselAll.UseVisualStyleBackColor = true;
            this.buttUnselAll.Click += new System.EventHandler(this.buttUnselAll_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 75);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(427, 22);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(43, 17);
            this.StatusLabel.Text = "Статус";
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToDeleteRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewResult.EnableHeadersVisualStyles = false;
            this.dataGridViewResult.Location = new System.Drawing.Point(417, 24);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.ReadOnly = true;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewResult.Size = new System.Drawing.Size(10, 273);
            this.dataGridViewResult.TabIndex = 19;
            this.dataGridViewResult.Visible = false;
            // 
            // openFileCSVDialog
            // 
            this.openFileCSVDialog.Filter = "CSV files(*.csv)|*.csv|All files(*.*)|*.*";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(427, 24);
            this.menuStrip.TabIndex = 20;
            this.menuStrip.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьФайлCSVToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьФайлCSVToolStripMenuItem
            // 
            this.открытьФайлCSVToolStripMenuItem.Name = "открытьФайлCSVToolStripMenuItem";
            this.открытьФайлCSVToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.открытьФайлCSVToolStripMenuItem.Text = "Открыть файл *.CSV";
            this.открытьФайлCSVToolStripMenuItem.Click += new System.EventHandler(this.открытьФайлCSVToolStripMenuItem_Click);
            // 
            // CheckB
            // 
            this.CheckB.DataPropertyName = "CheckB";
            this.CheckB.FalseValue = "";
            this.CheckB.FillWeight = 20F;
            this.CheckB.HeaderText = "";
            this.CheckB.IndeterminateValue = "";
            this.CheckB.MinimumWidth = 20;
            this.CheckB.Name = "CheckB";
            this.CheckB.ReadOnly = true;
            this.CheckB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CheckB.TrueValue = "";
            this.CheckB.Width = 40;
            // 
            // FormXML
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 394);
            this.Controls.Add(this.dataGridViewCSV);
            this.Controls.Add(this.dataGridViewResult);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(350, 350);
            this.Name = "FormXML";
            this.Text = "Генератор XML";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewCSV;
        private System.Windows.Forms.Button buttSelAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttUnselAll;
        private System.Windows.Forms.Button buttGenXML;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.Button buttShowResult;
        private System.Windows.Forms.OpenFileDialog openFileCSVDialog;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьФайлCSVToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckB;
    }
}

