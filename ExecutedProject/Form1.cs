using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json.Serialization;

namespace CreateXML
{
    public partial class FormXML : Form
    {
        DataSet set = new DataSet("AllTags");
        DataSet ds = new DataSet("root");
        DataTable TableType = new DataTable("TableType");
         DataTable TableItem = new DataTable("item");
         DataTable TableCSV = new DataTable("TableSCV");
         DataTable TableJSON = new DataTable("TableJSON"); // таблица, сформированная на основе файла JSON

        string path;
        string FileCSV;

        public class recTagJSON
        {
            public string TypeName { get; set; }    // имя типового элемента
            public string NameTag { get; set; }     // имя параметра
            public string TypeTag { get; set; }     // наменование типа параметра - ЛИШНИЙ (оставлен для проверки)
            public int lengthType { get; set; }     // размерность типа параметра в байтах
        }

        public class TypeInfo
        {
            public string TypeName { get; set; }
            public Dictionary<string, string /*Type*/> Propertys { get; set; }
        }

        public class RootObject
        {
            public List<TypeInfo> TypeInfos { get; set; }
        }

        private void ReadJSONFile(string pathToJSONFile)
        {
            // чтение данных из файла JSON
            recTagJSON rowListTag = new recTagJSON();
            try
            {
                StreamReader srJSON = new StreamReader(pathToJSONFile);
                string jsonString = srJSON.ReadToEnd();
                var listJSON = JsonConvert.DeserializeObject<RootObject>(jsonString);
                // Обработка элементов списка.
                // Формируем таблицу со столбцами 
                foreach (var itemTypeInfo in listJSON.TypeInfos)
                {
                    rowListTag.TypeName = itemTypeInfo.TypeName;
                    TableType.Rows.Add(rowListTag.TypeName);    // TableType
                    int SumLen = 0;
                    // Считываем имя свойства и его значение
                    foreach (var itemPropertys in itemTypeInfo.Propertys)
                    {
                        rowListTag.NameTag = itemPropertys.Key;
                        rowListTag.TypeTag = itemPropertys.Value;
                        switch (rowListTag.TypeTag)
                        {
                            case "double":
                                rowListTag.lengthType = 8; // 8 байт
                                break;
                            case "int":
                                rowListTag.lengthType = 4; // 4 байт
                                break;
                            case "bool":
                                rowListTag.lengthType = 2; // в c# bool - 1 байт, но в Modbus адресация к словам
                                break;
                            default:
                                rowListTag.lengthType = 0;
                                break;
                        }
                        SumLen = SumLen + rowListTag.lengthType;
                        TableJSON.Rows.Add(rowListTag.TypeName, rowListTag.NameTag, rowListTag.TypeTag, rowListTag.lengthType);
                        //System.Type.GetType(rowListTag.TypeTag)
                    }
                }
            }
            catch { }
        }

        private void ReadCSVFile(string pathToCsvFile)
        {
            int iStr = 0;
            using (StreamReader srCSV = new StreamReader(pathToCsvFile, Encoding.Default))
            {
                string[] headers = srCSV.ReadLine().Split(';');
                while (!srCSV.EndOfStream)
                {
                    iStr += 1;
                    string[] rows = srCSV.ReadLine().Split(';');
                    if (rows.Length == 3)
                    {
                        DataRow drCSV = TableCSV.NewRow();
                        drCSV[0] = 0;
                        for (int i = 0; i < headers.Length; i++)
                        {
                            drCSV[i + 1] = rows[i];
                        }
                        try {
                            TableCSV.Rows.Add(drCSV);
                        }
                        catch {
                            MessageBox.Show("Ошибка в файле CSV, в строке " + Convert.ToString(iStr) + ". Указанный тип не описан в JSON.\nСтрока исключена из обработки.","Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                        MessageBox.Show("Ошибка в файле CSV, в структуре строки " + Convert.ToString(iStr)+ ".\nСтрока исключена из обработки.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public FormXML()
        {
            InitializeComponent();

            // Get the current directory.
            path = Directory.GetCurrentDirectory();
            openFileCSVDialog.InitialDirectory = path;
            FileCSV = @path+@"\Files\Input.csv";

            ds.Tables.Add(TableItem);
            TableItem.Columns.Add("node-path", typeof(string));
            TableItem.Columns.Add("address", typeof(Int32));

            set.Tables.Add(TableJSON);
            set.Tables.Add(TableCSV);
            set.Tables.Add(TableType);

            TableCSV.Columns.Add("CheckB", System.Type.GetType("System.Boolean"));     // Добавление столбца для CheckBox в таблице
            TableCSV.Columns.Add("Tag", typeof(string));
            TableCSV.Columns.Add("Type", typeof(string));
            TableCSV.Columns.Add("Address", typeof(string));
            //TableCSV.Columns.Add("CheckB", System.Type.GetType("System.Boolean"));     // Добавление столбца для CheckBox в таблице
            TableCSV.Columns["CheckB"].DefaultValue = false;
            TableCSV.Columns.Add("IDcsv", System.Type.GetType("System.Int32"));   // Добавление столбца ID
            TableCSV.Columns["IDcsv"].Unique = true;
            TableCSV.Columns["IDcsv"].AllowDBNull = false;
            TableCSV.Columns["IDcsv"].AutoIncrement = true;
            TableCSV.Columns["IDcsv"].AutoIncrementSeed = 1;
            TableCSV.Columns["IDcsv"].AutoIncrementStep = 1;
            set.Tables["TableSCV"].PrimaryKey = new DataColumn[] { set.Tables["TableSCV"].Columns["IDcsv"] };

            TableJSON.Columns.Add("TypeName");                 // имя типового элемента
            TableJSON.Columns.Add("NameTag");                  // имя параметра
            TableJSON.Columns.Add("TypeTag");                  // наменование типа параметра - ЛИШНИЙ (оставлен для проверки)
            TableJSON.Columns.Add("lengthType", typeof(int));  // размерность типа параметра в байтах
            TableJSON.Columns.Add("IDjson", System.Type.GetType("System.Int32"));   // Добавление столбца ID
            TableJSON.Columns["IDjson"].Unique = true;
            TableJSON.Columns["IDjson"].AllowDBNull = false;
            TableJSON.Columns["IDjson"].AutoIncrement = true;
            TableJSON.Columns["IDjson"].AutoIncrementSeed = 1;
            TableJSON.Columns["IDjson"].AutoIncrementStep = 1;
            set.Tables["TableJSON"].PrimaryKey = new DataColumn[] { set.Tables["TableJSON"].Columns["IDjson"] };

            TableType.Columns.Add("TypeName");                 // имя типового элемента
            TableType.Columns.Add("ID", System.Type.GetType("System.Int32"));   // Добавление столбца ID
            TableType.Columns["ID"].Unique = true;
            TableType.Columns["ID"].AllowDBNull = false;
            TableType.Columns["ID"].AutoIncrement = true;
            TableType.Columns["ID"].AutoIncrementSeed = 1;
            TableType.Columns["ID"].AutoIncrementStep = 1;
            set.Tables["TableType"].PrimaryKey = new DataColumn[] { set.Tables["TableType"].Columns["ID"] };

            //создаем связь  TableCSV.Type <- TableType.TypeName -> TableJSON.TypeName
            set.Relations.Add("TypeJSON", TableType.Columns["TypeName"], TableJSON.Columns["TypeName"]);
            set.Relations.Add("TypeCSV", TableType.Columns["TypeName"], TableCSV.Columns["Type"]);

            //Заполнение таблиц TableCSV и TableJSON (Связи создаем до этого, что бы отслеживались несуществующие типы)
            if (File.Exists(path + @"\Files\TypeInfos.json"))
                ReadJSONFile(path + @"\Files\TypeInfos.json");
            if (File.Exists(FileCSV))
                ReadCSVFile(FileCSV);
            StatusLabel.Text = "Файлы CSV и JSON успешно загружены";

            TableType.Columns.Add("SUM", typeof(Int32), "SUM(Child(TypeJSON).lengthType)"); // суммарная размерность типа параметра в байтах

            set.AcceptChanges();

            dataGridViewCSV.DataSource = TableCSV;

            dataGridViewCSV.Columns[0].Width = 40;
            dataGridViewCSV.Columns[1].Width = 200;
            dataGridViewCSV.Columns[4].Visible = false;
            this.Width = 476;

            dataGridViewResult.Width = 347; //320
            dataGridViewResult.DataSource = TableItem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dataGridViewCSV.Rows)
                item.Cells["CheckB"].Value = true;
            CalcColumnAddress();
        }

        private void buttUnselAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dataGridViewCSV.Rows)
                item.Cells["CheckB"].Value = false;
            CalcColumnAddress();
        }
        private void dataGridViewCSV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)  // реакция на нажатие, кроме первой строки (что бы работала сортировка)
            {
                //нажатие в CheckBox
                if ((bool)dataGridViewCSV.Rows[dataGridViewCSV.CurrentRow.Index].Cells["CheckB"].Value) {
                    dataGridViewCSV.Rows[dataGridViewCSV.CurrentRow.Index].Cells["CheckB"].Value = false;
                }
                else {
                    dataGridViewCSV.Rows[dataGridViewCSV.CurrentRow.Index].Cells["CheckB"].Value = true;
                }
                CalcColumnAddress();
            }
        }

        private void CalcColumnAddress()
        {
            dataGridViewCSV.DataSource = null;
            set.AcceptChanges();
            int CTAddress = 0;
            foreach (DataRow row in TableCSV.Rows)
            {
                if (row.Field<bool>("CheckB"))
                {
                    row.SetField<int>(TableCSV.Columns.IndexOf("Address"), CTAddress);
                    CTAddress = CTAddress + row.GetParentRow("TypeCSV").Field<int>("SUM"); 
                }
                else row.SetField<int?>(TableCSV.Columns.IndexOf("Address"), null);
            }

            dataGridViewCSV.DataSource = TableCSV;
            dataGridViewCSV.Columns[0].Width = 40;
            dataGridViewCSV.Columns[0].HeaderText = "";
            dataGridViewCSV.Columns[1].Width = 200;
            dataGridViewCSV.Columns[4].Visible = false;

            StatusLabel.Text = "Выбор элементов обработан.";

            if (dataGridViewResult.Visible)
            {
                CreateResult();
            }
        }

        private void CreateResult()
        {
            dataGridViewResult.DataSource = null;
            string filter;
            string sTag;
            string sType;
            string sNameTag;
            int iLengthType;
            int CTAddress;

            TableItem.Clear();
            CTAddress = 0;
            foreach (DataRow row in TableCSV.Select("CheckB = True"))
            {
                sTag = row.Field<string>("Tag");
                sType = row.Field<string>("Type");         
                filter = "TypeName = '" + sType + "'";
                foreach (DataRow rowJSON in TableJSON.Select(filter))
                {
                    sNameTag = rowJSON.Field<string>("NameTag");
                    TableItem.Rows.Add(sTag + "." + sNameTag, CTAddress);
                    iLengthType = rowJSON.Field<int>("lengthType");
                    CTAddress = CTAddress + iLengthType;
                }
            }
            dataGridViewResult.DataSource = TableItem;
            StatusLabel.Text = "Выбор элементов обработан, итоговая таблица сформирована.";
        }

        private void buttGenXML_Click(object sender, EventArgs e)
        {
            // сохранение файла
            saveFileDialog.Filter = "XML files(*.xml)|*.xml";
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                StatusLabel.Text = "Сохранение файл XML отменено.";
                return;
            }
            // получаем выбранный файл
            string filenameXML = saveFileDialog.FileName;

            CreateResult();
            //BindingSource bs = new BindingSource();
            //bs.Add(ds);
            //ds.WriteXml(path + @"\Files\result.xml");

            XDocument xdoc = new XDocument();
            // создаем корневой элемент
            XElement root = new XElement("root");
            foreach (DataRow row in TableItem.Rows)
            {
                XElement item = new XElement("item",
                    new XAttribute("Binding", "Introduced"),
                    new XElement("node-path", row.Field<string>("node-path")),
                    new XElement("address", row.Field<int>("address")));
                // добавляем в корневой элемент
                root.Add(item);
            }
            // добавляем корневой элемент в документ
            xdoc.Add(root);
            //сохраняем документ
            xdoc.Save(filenameXML);
            StatusLabel.Text = "Файл XML успешно сформирован и сохранен.";
        }

        private void buttShowResult_Click(object sender, EventArgs e)
        {
            dataGridViewResult.Visible = !dataGridViewResult.Visible;
            if (dataGridViewResult.Visible)
            {
                CreateResult();
                buttShowResult.Text = "Результат <<";
                this.Width = this.Width + dataGridViewResult.Width;
            }
            else
            {
                buttShowResult.Text = "Результат >>";
                this.Width = this.Width - dataGridViewResult.Width;
            }
        }

        private void открытьФайлCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileCSVDialog.ShowDialog() == DialogResult.Cancel)
                return;
            FileCSV = openFileCSVDialog.FileName;
            
            if (File.Exists(FileCSV))
            {
                TableCSV.Clear();
                ReadCSVFile(FileCSV);
                StatusLabel.Text = "Файл CSV успешно загружен.";
            }
        }


        //--------------------------------------------------------------------------------------------------------------
        //------------------------------Для пробы. В программе не используется------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        // класс и его члены объявлены как public
        [Serializable]
        public class item
        {
            /* The XML element name will be XName instead of the default ClassName. */
            [XmlElement(ElementName = "node-path")]
            public string nodepath { get; set; }
            [XmlElement(ElementName = "address")]
            public int address { get; set; }


            // стандартный конструктор без параметров
            public item()
            { }

            public item(string NodePath, int Address)
            {
                nodepath = NodePath;
                address = Address;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            item item1 = new item { nodepath = "root.SSN1.ZRP1.flowkD", address = 100 };
            item item2 = new item { nodepath = "root.SSN1.ZRP1.flowkI", address = 101 };
            // объект для сериализации
            item[] root = new item[] { item1, item2 };
            // передаем в конструктор тип класса
            XmlSerializer formatter = new XmlSerializer(typeof(item[]));
            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("rezult3.xml", FileMode.OpenOrCreate))
            {
                 formatter.Serialize(fs, root);
            }

            XDocument xdoc = new XDocument(new XElement("root",
                new XElement("item",
                    new XAttribute("Binding", "Introduced"),
                    new XElement("node-path", "root.SSN1.ZRP1.flowkD"),
                    new XElement("address", "100")),
                new XElement("item",
                    new XAttribute("Binding", "Introduced"),
                    new XElement("node-path", "root.SSN1.ZRP1.flowkI"),
                    new XElement("address", "101"))));
            xdoc.Save("phones.xml");

        }
    }
}
